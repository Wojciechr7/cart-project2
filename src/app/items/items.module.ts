import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SaladsComponent } from './salads/salads.component';
import { DessertsComponent } from './desserts/desserts.component';
import { BeveragesComponent } from './beverages/beverages.component';
import {MatCardModule} from '@angular/material/card';
import { BurgersComponent } from './burgers/burgers.component';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes =[
  {
    path: 'burgers', component: BurgersComponent
  },
  {
    path: 'salads', component: SaladsComponent
  },
  {
    path: 'desserts', component: DessertsComponent
  },
  {
    path: 'beverages', component: BeveragesComponent
  }
]
@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [
  ]
})
export class ItemsModule { }
