export interface Products {
    id:number
    name:string
    cost:number
    description:string
    image:string
}
