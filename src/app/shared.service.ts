import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class SharedService {
  

  readonly APIUrl="http://localhost:3000/api"
  constructor(private http:HttpClient) { }

  getBurgerList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/burgers');
  }
  getSaladList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/salads');
  }
  getDessertList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/desserts');
  }
  getBeverList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/beverages');
  }
  getUserList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/users');
  }
  
}